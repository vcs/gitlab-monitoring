#! /bin/sh

# Use nss_wrapper to add the current user to /etc/passwd
# and enable the use of tools like git
USER_ID=$(id -u)
GROUP_ID=$(id -g)
# Pointless if running as root
if [[ "${USER_ID}" != '0' ]]; then
  export NSS_WRAPPER_PASSWD=/tmp/nss_passwd
  export NSS_WRAPPER_GROUP=/tmp/nss_group
  cp /etc/passwd $NSS_WRAPPER_PASSWD
  cp /etc/group  $NSS_WRAPPER_GROUP

  if ! getent passwd "${USER_ID}" >/dev/null; then
     # we need an entry in passwd for current user. Make sure there is no conflict
     sed -e '/^gitlabsls:/d' -i $NSS_WRAPPER_PASSWD
     echo "gitlabsls:x:${USER_ID}:${GROUP_ID}:Gitlab sls:/gitlab-monitoring:/sbin/nologin" >> $NSS_WRAPPER_PASSWD
  fi

  export LD_PRELOAD=libnss_wrapper.so
fi

echo " * Testing GitLab"
export HOME=/gitlab-monitoring
cd $HOME
echo "$GITLABSLS_PWD" | kinit $GITLABSLS_USER@CERN.CH
mkdir -p $HOME/.ssh
# We replace the username and path to use on the ssh connection
envsubst < /gitlab-monitoring/ssh-config > $HOME/.ssh/config
chmod 644 $HOME/.ssh/config

export PYTHONPATH=$PYTHONPATH:./.pip
python /gitlab-monitoring/generateCombinedJSON.py #--only-print

echo " * (DONE) Testing GitLab"
