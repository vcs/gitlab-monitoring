#!/bin/bash

# Provisional script till we get a proper sensor
# Check the occupancy of the NFS filer and send alerts by mail
# Borja Aparicio July 2015

DF="/usr/bin/df"
if [ ! -x $DF ]
then
	echo ERROR: \"$DF\" not found in the system
	exit 1
fi

MAIL="/usr/bin/mail"
if [ ! -x $MAIL ]
then
	echo ERROR: \"$MAIL\" not found in the system
	exit 1
fi


# Directory to monitor
dir="/var/lib/git/git-data/"
# % limit to send the alert
limit=80
# Mail where to send the alert
mail_to="it-service-vcs@cern.ch"
# Flag to not send the mail
not_send=0

HOSTNAME=`hostname`

help () {
        echo "Usage: $0 [--dir /directory/to/monitor] [--limit number] [--mail name@cern.ch] [--not-send] [--help]"
	echo "	--dir: Directory to monitor. Defaul: /var/lib/git/git-data/"
	echo "	--limit: % after we send the warning by maili. Default, 80. Must be (0<limit<100)"
	echo "	--mail: Email address where to send the warning. Default it-service-vcs@cern.ch"
	echo "	--not-send: Skips the mail. Only standard output"
}


while [ $# -gt 0 ] 
do
        case $1 in
        "-d"|"--dir")
                dir=$2
                shift
                shift
        ;;
        "-l"|"--limit")
                limit=$2
                shift
                shift
        ;;
        "-m"|"--mail")
		mail_to=$2
                shift
        ;;
        "-n"|"--not-send")
                not_send=1
                shift
	;;
        "--help")
                help
                exit 0
        ;;
        *)
                echo "unknown option in $0 : $1"
                help
                exit 1
        ;;
        esac
done

if [ ! -d $dir ]
then
	echo "ERROR: Directory don\'t exist: $dir"
	exit 1
fi

if [[ $limit -le 0 || $limit -ge 100 ]]
then
	echo "ERROR: Limit has to be (0<limit<100)"
	exit 1
fi


usage=$($DF -H $dir --output=pcent | grep -v "Use" | sed s/%// | sed s/" "//)

message="$HOSTNAME running out of space:

The filer $dir
in $HOSTNAME
is $usage full.
Request an increase of quota.

VCS Team,"

if [[ $limit -le $usage ]]
then
	if [ $not_send -eq 1 ]
	then
		echo "WARNING: $HOSTNAME:$dir full at $usage%"
	else
		echo -e "$message" | $MAIL -s "$HOSTNAME: NFS filer at $usage%" -r no-reply@cern.ch $mail_to 
	fi
fi