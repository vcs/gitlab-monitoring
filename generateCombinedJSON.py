#!/usr/bin/python

'''
    Generates and sends the SLS XML
'''

import sys
import argparse
import datetime
import logging
import time
import logger
import requests
import json
import subprocess
import os
import http.client

from cernservicexml import Status


SERVICE_ID = 'GIT-COMBINED'
GITLAB = 'gitlab.cern.ch'
PRODUCER = 'git'
SLS_ENDPOINT = 'http://monit-metrics.cern.ch:10012/'

LOGGER = logger.Logger(SERVICE_ID)

form = '%(levelname)s %(name)s:%(asctime)-15s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO, format=form)
# Silence logging from requests library
logging.getLogger("requests").setLevel(logging.WARNING)

def send(document):
    return requests.post(SLS_ENDPOINT, data=json.dumps(document), headers={ "Content-Type": "application/json; charset=UTF-8"})

def send_and_check(document, should_fail=False):
    response = send(document)
    assert( (response.status_code in [200]) != should_fail), 'With document: {0}. Status code: {1}. Message: {2}'.format(document, response.status_code, response.text)


####################
# Ping the service.
####################
def get_ping_response(instance):
    '''
    Ping the service.
    '''
    return os.system("ping -c 1 " + instance + " > /dev/null")

def get_http_response(instance_url):
    '''
    GET HTTP response, similar to Ping
    '''
    http_response = False
    try:
        connection = http.client.HTTPSConnection(
            host=instance_url, port=443, timeout=10)
        connection.request("GET", "/")
        response = connection.getresponse()
        if response.status == 302:
            http_response = True
        LOGGER.getLogger().info('Got response status: ' +str(response.status))
    except http.client.HTTPException:
        LOGGER.getLogger().error('GET http://'+instance_url+'/ failed with HTTPException',
                                 exc_info=True)
    except:
        LOGGER.getLogger().error('GET http://'+instance_url+'/ failed')
    return http_response

#####################################################
# Ping the API. We don't really need to authenticate.
# A return of 401 means that the API is replying.
#####################################################
def get_api_response(instance_url):
    '''
    Ping the API. We don't really need to authenticate.
    A return of 401 means that the API is replying.
    '''
    api_response = False
    try:
        connection = http.client.HTTPSConnection(
            host=instance_url, port=443, timeout=10)
        connection.request("GET", "/api/v4/user")
        response = connection.getresponse()
        if response.status == 401:
            api_response = True
        LOGGER.getLogger().info('Got API response status: ' +str(response.status))
    except http.client.HTTPException:
        LOGGER.getLogger().error('API ping failed with HTTPException',
                                 exc_info=True)
    except:
        LOGGER.getLogger().error('API ping failed', exc_info=True)
    return api_response



####################################################################
# Check SSH access by doing an ls-remote. It relies in gitlabsls user.
# The access to the project is based the project membership and a
# public key stored on the Gitlab profile.
####################################################################
def get_ssh_response(instance_url):   
    '''
    Check SSH access by doing an ls-remote. It relies in gitlabsls user.
    The access to the project is based the project membership and a
    public key stored on the Gitlab profile.
    '''
    ssh_response = True

    output = ''
    err = ''

    command = ("timeout 10 git ls-remote ssh://git@"
               + instance_url +":7999/vcs/gitlab-monitoring.git")
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE,
                               shell=True, preexec_fn=os.setsid)
    output, err = process.communicate()

    if process.returncode == 124: # 124 = timeout
        LOGGER.getLogger().error('ls-remote timed out')

    if process.returncode != 0:
        ssh_response = False
        LOGGER.getLogger().error('ls-remote had return code ' +str(process.returncode))

        if output:
            LOGGER.getLogger().error('ls-remote output: ' +str(output))
        if err:
            LOGGER.getLogger().error('ls-remote error: ' +str(err))

    return ssh_response

def get_gitlab_service_availability(instance_url):
    '''
        Return availability of a given GitLab instance
    '''
    LOGGER.getLogger().info('Getting http response.')
    http_response = get_http_response(instance_url)
    LOGGER.getLogger().info('Finished http response.')

    LOGGER.getLogger().info('Getting API response.')
    api_response = get_api_response(instance_url)
    LOGGER.getLogger().info('Finished API response.')

    LOGGER.getLogger().info('Getting SSH response.')
    #ssh_response = get_ssh_response(instance_url)
    ssh_response = True
    LOGGER.getLogger().info('Finished SSH response.')
    
    service_availability = {}

    if not http_response:
        LOGGER.getLogger().error('Ping response was: ' +str(http_response))
        service_availability['status'] = Status.unavailable
        service_availability['info_message'] = "GitLab ping failed, service unavailable"
    elif api_response and ssh_response:
        service_availability['status'] = Status.available
        service_availability['info_message'] = "GitLab is available"
    elif not api_response and ssh_response:
        service_availability['status'] = Status.degraded
        service_availability['info_message'] = "GitLab web access degraded"
    elif api_response and not ssh_response:
        # If a Git command doesn't work, we consider GitLab unavailable
        service_availability['status'] = Status.unavailable
        service_availability['info_message'] = "GitLab SSH access degraded"
    elif not api_response and not ssh_response:
        service_availability['status'] = Status.unavailable
        service_availability['info_message'] = "GitLab service unavailable"

    return service_availability

# Parse argument if there are some
PARSER = argparse.ArgumentParser(description="Generate GIT-combined JSON availability")
PARSER.add_argument(
    '-o', '--only-print',
    action="store_true",
    help="Only print JSON, don't send it.")
PARSER.add_argument(
    '-l', '--gitlab_id',
    help="Set test ID for GitLab, useful when testing to send to Kibana.")
PARSER.add_argument(
    '-c', '--git_combined',
    help="Set test ID for git-combined, useful when testing to send to Kibana.")

ARGS = PARSER.parse_args()

if __name__ == '__main__':

    if ARGS.gitlab_id:
        gitlab_id = ARGS.gitlab_id
    if ARGS.git_combined:
        git_combined = ARGS.git_combined
    
    gitlab_instance_url = GITLAB

    # Logging
    LOGGER.getLogger().info("Starting test GIT-COMBINED -------------------------")
    

    LOGGER.getLogger().info("--- Availability for instance: gitlab.cern.ch")
    
    # Availability for gitlab.cern.ch instance
    gitlab_cern_ch_availability = get_gitlab_service_availability(gitlab_instance_url)

    # Try again once if one service is degraded to avoid false errors.
    if  ("status" in gitlab_cern_ch_availability) and (gitlab_cern_ch_availability["status"] != Status.available):
        LOGGER.getLogger().info('gitlab.cern.ch was degraded or unavailable, retrying...')
        time.sleep(30)
        gitlab_cern_ch_availability = get_gitlab_service_availability(gitlab_instance_url)

    # Log the availability
    LOGGER.getLogger().info('Availability info: %s', gitlab_cern_ch_availability)
    
    
   
    #if we get status availability information from previous call, otherwise unexpected error
    if "status" in gitlab_cern_ch_availability:
        gitlab_cern_ch_availability_json = [{
            "producer": PRODUCER,
            "type": "availability",
            "timestamp": str(round(time.time()*1000)),
            "serviceid": SERVICE_ID,
            "service_status": str(gitlab_cern_ch_availability["status"]),
            "availabilitydesc": str(gitlab_cern_ch_availability["info_message"]),
            "availabilityinfo": str(gitlab_cern_ch_availability["status"]),
            "webpage": 'https://information-technology.web.cern.ch/services/git-service',
            "contact": 'https://cern.service-now.com/service-portal?id=service_element&name=git-service',
        }]
 
        LOGGER.getLogger().info("JSON to be send:" + str(json.dumps(gitlab_cern_ch_availability_json, indent=3)))

        if ARGS.only_print:
            print('Not sending...')
            print(str(json.dumps(gitlab_cern_ch_availability_json, indent=3)))
        else:
            LOGGER.getLogger().info("Sending to MONIT...")
            send_and_check(gitlab_cern_ch_availability_json)

        LOGGER.getLogger().info("Finished test GIT-COMBINED -------------------------")
        sys.exit(0)
    else:
        LOGGER.getLogger().info("Exiting test GIT-COMBINED (GOT NO GITLAB STATUS INFORMATION) -------------------------")
        sys.exit(1)
