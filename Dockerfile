#
FROM gitlab-registry.cern.ch/linuxsupport/cc7-base
#
MAINTAINER Daniel Juarez Gonzalez <djuarezg@cern.ch>

ARG pip_path=/gitlab-monitoring/.pip
#
# Python modules
#
COPY requirements.txt .

ENV PATH=/opt/rh/rh-python36/root/usr/bin/:$PATH

RUN INSTALL_PKGS="rh-python36-python git svn cern-get-sso-cookie nss_wrapper xmlstarlet gettext python-requests python-requests-kerberos bc openssh-clients cern-wrappers" && \
    INSTALL_PKGS_BUILD="rh-python36-python-devel" && \
    yum install -y centos-release-scl && \
    yum update -y && \
    yum install -y --setopt=tsflags=nodocs --enablerepo=centosplus $INSTALL_PKGS && \
    # Only for pip build/install
    yum install -t -y $INSTALL_PKGS_BUILD && \
    mkdir -p $pip_path && \
    pip install -t $pip_path --upgrade && \
    pip install -t $pip_path -r requirements.txt && \
    # Remove requirements.txt, only used for install
    rm requirements.txt && \
    yum remove -y $INSTALL_PKGS_BUILD && \
    yum clean all -y && \
    rm -rf /var/cache/yum /var/log/anaconda/ /var/lib/yum/ \
    /var/lib/rpm/ /root/.cache/


COPY . /gitlab-monitoring

RUN chown -R 1001:0 /gitlab-monitoring && \
    chmod -R ug+rwx /gitlab-monitoring

WORKDIR /gitlab-monitoring/

ENTRYPOINT ["./run.sh"]
