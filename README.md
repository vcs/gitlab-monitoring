Elements included:
* XML (for SLS) generation.
  * generateGitLabXml.py
  * generateCombinedXml.py
  * generateGitXml.py

* The XML is generated and sent using the [cernservicexml](http://cernservicexml.readthedocs.org/en/latest/) module.

* The `generateCombinedXml.py` script combines the XML from both Git and GitLab, and sends an XSLS status update for both services and a combined status.
  
* The availability is created by a `gitlabsls` cronjob on Openshift in the `gitlab` namespace (See https://gitlab.cern.ch/vcs/oo-gitlab-cern/merge_requests/98).

  * Meter view [combined](https://meter.cern.ch/public/_plugin/kibana/#/dashboard/elasticsearch/Metrics:%20Availability?query=git-combined)

  * Meter view [GitLab](https://meter.cern.ch/public/_plugin/kibana/#/dashboard/elasticsearch/Metrics:%20Availability?query=gitlab)

  * Meter view [GIT](https://meter.cern.ch/public/_plugin/kibana/#/dashboard/elasticsearch/Metrics:%20Availability?query=git)


* Script to check usage of NFS filers:
  * check-nfs-usage.sh

## How to run this locally for testing


Edit run.sh to execute using --only-print

docker build -t gitlab-monitoring .

Prepare ssh key (get information from Openshift gitlab deployment (config map)) (copy content for example on /root/private/gitlab-sls-ssh-key on your VM)

# get password from Openshift prod
GITLABSLS_PWD=$(oc get configmap/gitlab-secrets -o "jsonpath={ .data['gitlab-sls-pwd']}")

docker run -u 1001100000:0 -v /root/private/gitlab-sls-ssh-key:/gitlab-monitoring/gitlab-sls-ssh-key  --env-file ./env.list -e GITLABSLS_PWD=${GITLABSLS_PWD} gitlab-monitoring:latest


