'''
	Logging class
'''
import logging
import sys

class Logger(object):
    '''
    Logging class
    '''
    def __init__(self, service):
        '''
        Constructor
        '''
        self.service = service
        self.servicelogger = None
        self.handler = None

    def getLogger(self):
        '''
        Returns an instance of the logger
        '''
        form = logging.Formatter('%(levelname)s '+self.service+':%(asctime)-15s %(message)s')
        logging.basicConfig(format=form)

        if not self.handler:
            self.servicelogger = logging.getLogger(self.service)
            self.handler = logging.StreamHandler(sys.stdout)
            self.handler.setFormatter(form)
            self.handler.setLevel(logging.INFO)
        return self.servicelogger
